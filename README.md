## 0. Resolução


**IMPORTANTE**: O token é de uso pessoal e **NÃO DEVE SER COMPARTILHADO**. As imagens a seguir exibem um token falso.

1. Criar uma conta no portal de dados do governo e pegar seu token: http://www.transparencia.gov.br/api-de-dados

![Token](var/resolucao/atividade_1.png "Pegando o token para acesso")

2. Adicionar o token na aplicação

![Token App](var/resolucao/atividade_2.png "Adicionando o token na aplicação")


3. Executar um teste de integração

Antes de executar esta etapa, é necessário inicializar a aplicação, instalar dependências e rodar migrações.

Os comandos que devem ser executados são:

```
docker-compose up
```

Em outra aba de terminal

```sh
docker exec -it tvas-php bash # acessa o contêiner do php
composer install # instala dependências da aplicação
vendor/bin/phinx migrate # cria tabela inicial (siafi_codes)
```
Obs.: Todos estes comandos estão listados na seção 3 deste arquivo.

![Teste](var/resolucao/atividade_3_1.png "Preparando a aplicação")

Após estes comandos, é possível executar o teste. Note que, primeiro foi executado o teste com uma chave inválida e a após inserir a chave correta o teste passou.

![Teste](var/resolucao/atividade_3_2.png "Executando um teste de integração")

4. Criar um service para consulta na transparência para os endpoints:

- **endpoint1**: GET /api-de-dados/auxilio-emergencial-por-municipio
- **endpoint2**: GET /api-de-dados/bpc-por-municipio

O _service_ pode ser visto em [BpcAndEmergencyAidService](src/Service/BpcAndEmergencyAidService.php). Note que foi adicionada uma entrada para ele no arquivo [dependencies.php](config/dependencies.php). Note também que, a construção do cliente http foi separada em uma função [createClient](src/Helper/functions.php) para evitar duplicação de código.

O service possui dois métodos publicos (além do construtor) um para cada endpoint solicitado. Em caso de erro, cada método retornará uma exceção de domínio com código diferente isso facilita a documentação dos erros possíveis da aplicação.

5. Criar duas tabelas, uma para cada endpoint. Os campos das tabelas deverão ser criados de acordo com os campos de cada endpoint

Para criar tabelas devemos utilizar o phinx e os campos devem ser escolhidos de acordo com a documentação da Transparência.

```
vendor/bin/phinx create EmergencyAidOrBpc
```

A migração criada pode ser vista em:

- [EmergencyAidOrBpc](data/database/migrations/20210412225126_emergency_aid_or_bpc.php)

**Importante**: como é possível notar, os retornos de ambos os endpoints são iguais, mudando apenas o tipo de benefício.

Por exemplo, para o município de Itajubá (código ibge = `3132404)` no mesAno `202004` tem-se:

- **endpoint1**: GET /api-de-dados/auxilio-emergencial-por-municipio

```json
[
  {
    "id": 105201877,
    "dataReferencia": "2020-04-01",
    "municipio": {
      "codigoIBGE": "3132404",
      "nomeIBGE": "ITAJUBÁ",
      "codigoRegiao": "3",
      "nomeRegiao": "SUDESTE",
      "pais": "BRASIL",
      "uf": {
        "sigla": "MG",
        "nome": "MINAS GERAIS"
      }
    },
    "tipo": {
      "id": 6,
      "descricao": "Auxílio Emergencial",
      "descricaoDetalhada": "Auxílio Emergencial"
    },
    "valor": 11966400,
    "quantidadeBeneficiados": 17614
  }
]
```

- **endpoint2**: GET /api-de-dados/bpc-por-municipio

```json
[
  {
    "id": 98309418,
    "dataReferencia": "2020-04-01",
    "municipio": {
      "codigoIBGE": "3132404",
      "nomeIBGE": "ITAJUBÁ",
      "codigoRegiao": "3",
      "nomeRegiao": "SUDESTE",
      "pais": "BRASIL",
      "uf": {
        "sigla": "MG",
        "nome": "MINAS GERAIS"
      }
    },
    "tipo": {
      "id": 5,
      "descricao": "BPC",
      "descricaoDetalhada": "Benefício de Prestação Continuada"
    },
    "valor": 2214358.25,
    "quantidadeBeneficiados": 2117
  }
]
```

Como os campos são os mesmos, diferindo apenas no tipo de benefício, será feita uma tabela só. Nesta prova não há problema em fazer duas (mas no dia-a-dia duplicação de código não é legal), criar apenas uma tabela e consultar pelo tipo ou pelos tipos é mais fácil =).

Após criar a migração é necessário executá-la

```
vendor/bin/phinx migrate
```


6. Criar um rota na aplicação que dado um *codigoIbge* e um valor *mesAno*

A rota para ser acessada através de uma chamada http de dentro do contêiner (somente de dentro do contêiner).

```
curl "http://localhost:8080/transparencia/bpc-and-emergency-aid?ibge_code=3132404&month_year=202004&page_number=1"
```

![Erro Requisição](var/resolucao/atividade_6_1.png)


Observe que ao executar teste teste obtemos a mensagem de chave inválida. Isso acontece, pois nossa chave foi colocada apenas em ambiente de teste. Para usarmos fora dos testes precisamos alterar o arquivo config/settings.php para que o valor venha de lá.

Ex:

```php
// config/settings.php
// coloque a chave momentaneamente para realizar a requisição e ver que está funcionando.
'transparencia.token' => 'meu-token-secreto',
```

agora ao executar o curl

![Requisição com sucesso](var/resolucao/atividade_6_2.png)

Observe que a rota criada satisfaz as três condições indicadas:

1. Consulta na transparência ou na base de dados (se os valores já existirem na base de dados). Ver model [BpcAndEmergencyAid](src/Model/BpcAndEmergencyAid.php)
2. Salva consultas na base de dados.

![Requisição combinada](var/resolucao/atividade_6_3.png)

3. Retorna os valores combinados das duas consultas.

![Requisição combinada](var/resolucao/atividade_6_4.png)

## 1. Atividades

Observação: candidatos à vaga de estágio deverão fazer pelo menos os itens 1 a 6. Candidatos para contratação deverão concluir todos os itens.

1. Criar uma conta no portal de dados do governo e pegar seu token: http://www.transparencia.gov.br/api-de-dados
2. Adicionar o token na aplicação, conforme exemplo abaixo:

```php
<?php

// tests/settings.local.php
// Este arquivo é ignorado pelo git. Sendo assim, deverá ser criado pelo candidato.

return [
    'transparencia.token' => 'meu-token-secreto',
];
```

3. Executar um teste de integração para verificar se a conexão está ok.

```sh
vendor/bin/phpunit --filter testGetSiafiCodesIntegration

# esperado:
# OK (1 test, 1 assertion)
```

4. Criar um service para consulta na transparência para os endpoints:

- **endpoint1**: GET /api-de-dados/auxilio-emergencial-por-municipio
- **endpoint2**: GET /api-de-dados/bpc-por-municipio

5. Criar duas tabelas, uma para cada endpoint. Os campos das tabelas deverão ser criados de acordo com os campos de cada endpoint

6. Criar um rota na aplicação que dado um *codigoIbge* e um valor *mesAno*:

- Consulta nos dois enpoints do item 4 (somente se a consulta já não estiver salva nas tabelas) ou utiliza os valores já salvos no banco de dados.
- Salva nas duas tabelas do item 5 (somente se a consulta já não estiver salva nas tabelas).
- Retorna os valores combinados das duas consultas

Observação: O código IBGE escolhido ficará a critério do candidato (escolha um código que traga resultados).

7. Realizar consultas para os meses de 2020 (janeiro a dezembro) nos dois enpoints e criar uma *seed* usando o phinx.

8. Construir testes:

- Um teste de integração para cada método do service.
- Um teste de integração pra cada repositório criado.
- Um teste funcional para a rota criada.
- Um teste unitário para o model e para o service criado.

9. Documentar rotas utilizando alguma especificação de API. Exemplo: https://swagger.io/docs/specification/about/

## 2. Considerações

- O estilo de código deverá seguir as PSR's 1 e 12.
- A injeção de dependências deverá seguir a PSR 11.
- Documentação sobre as psr's: https://www.php-fig.org/psr/
- Documentação do Slim: https://www.slimframework.com/docs/v4/
- Documentação do Phinx: https://phinx.readthedocs.io/en/latest/intro.html
- A entrega da prova deverá ser através de compartilhamento de um repositório privado (no gitlab) com o usuário @incluirtecnologia e link da documentação da API no Swagger (https://swagger.io/).

## 3. Comandos Úteis

1. Iniciar o docker

```sh
docker-compose up
```

2. Entrar no contêiner do php

```sh
docker exec -it tvas-php bash
```

3. Instalar dependências

```sh
# dentro do contêiner do php
composer install
```

4. Executar migrações no banco de dados

```sh
# dentro do contêiner do php
vendor/bin/phinx migrate
```

5. Acessar o container do banco de dados

```sh
docker exec -it tvas-sql bash -c "mysql -u tvas -p'tvas' tvas"
```

6. Acessar log da aplicação

Observação: antes de executar o comando abaixo, crie um arquivo vazio em `var/logs/app.log`.

```sh
docker exec -it tvas-php bash -c "grc -c conf.log tail -f var/logs/app.log"
```

7. Executar um teste específico

```sh
# dentro do contêiner do php
vendor/bin/phpunit --filter <ClasseDeTesteOuMetodoDeTeste>
# exemplo
vendor/bin/phpunit --filter testHealthCheckWillReturn200StatusCode
```

Mais informações em: https://phpunit.readthedocs.io/en/9.5/

8. Executar análise estática

```php
vendor/bin/psalm --show-info=true
```

Mais informações em: https://phinx.readthedocs.io/en/latest/intro.html

## 4. Documentação da API da Transparência

- http://www.transparencia.gov.br/api-de-dados