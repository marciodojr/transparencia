<?php

use Phinx\Migration\AbstractMigration;

class EmergencyAidOrBpc extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this
            ->table('aid')
            ->addColumn('id_ref', 'integer', [
                'null' => false,
            ])
            ->addColumn('data_referencia', 'date', [
                'null' => false,
            ])
            ->addColumn('municipio_codigo_ibge', 'string', [
                'limit' => 20,
                'null' => true
            ])
            ->addColumn('municipio_nome_ibge', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('municipio_codigo_regiao', 'integer', [
                'null' => true
            ])
            ->addColumn('municipio_nome_regiao', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('municipio_pais', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('municipio_uf_sigla', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('municipio_uf_nome', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('tipo_id', 'integer', [
                'null' => true
            ])
            ->addColumn('tipo_descricao', 'string', [
                'limit' => 255,
            ])
            ->addColumn('tipo_descricao_detalhada', 'string', [
                'limit' => 255,
            ])
            ->addColumn('valor_centavos', 'integer', [
                'null' => true,
            ])
            ->addColumn('quantidade_beneficiados', 'integer', [
                'null' => true,
            ])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => 'CURRENT_TIMESTAMP',
            ])
            ->addIndex('data_referencia')
            ->addIndex('municipio_codigo_ibge')
            ->addIndex('tipo_id')
            ->addIndex(['data_referencia', 'municipio_codigo_ibge', 'tipo_id'], [
                'unique' => true,
            ])
            ->create();
    }
}
