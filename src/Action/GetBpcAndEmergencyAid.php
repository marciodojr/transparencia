<?php

namespace Intec\TransparenciaViagensServico\Action;

use DateTime;
use Intec\IntecSlimBase\Action\JsonAction;
use Intec\TransparenciaViagensServico\Model\BpcAndEmergencyAid;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class GetBpcAndEmergencyAid extends JsonAction
{
    public function __construct(private BpcAndEmergencyAid $bpcAndEmergencyAid)
    {
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = $request->getQueryParams();

        $bpcAndEmergencyAid = $this->bpcAndEmergencyAid->getBpcAndEmergencyAidByCity(
            $params['ibge_code'],
            DateTime::createFromFormat('Ym', $params['month_year']),
            $params['page_number']
        );

        return $this->toJson(response: $response, data: $bpcAndEmergencyAid);
    }
}
