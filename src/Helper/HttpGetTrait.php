<?php

namespace Intec\TransparenciaViagensServico\Helper;

use Exception;
use GuzzleHttp\Exception\BadResponseException;

trait HttpGetTrait
{
    private function get(string $path, array $queryParams): ?array
    {
        try {
            $response = $this->httpClient->request('GET', $path, [
                'query' => $queryParams,
            ]);

            return $response->getStatusCode() != 204
                ? json_decode((string) $response->getBody(), true)
                : null;
        } catch (BadResponseException $e) {
            $message = (string) ($e->getResponse())->getBody();
            if (!$message) {
                $message = $e->getMessage();
            }

            throw new Exception($message, 0, $e);
        }
    }
}
