<?php

namespace Intec\TransparenciaViagensServico\Helper;

use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

function createClient(ContainerInterface $container): Client
{
    $token = $container->get('transparencia.token');

    return new Client([
        'base_uri' => 'http://api.portaldatransparencia.gov.br',
        'headers' => [
            'chave-api-dados' => $token,
        ],
    ]);
}
