<?php

namespace Intec\TransparenciaViagensServico\Model;

use DateTime;
use Intec\TransparenciaViagensServico\Repository\BpcAndEmergencyAidRepository;
use Intec\TransparenciaViagensServico\Service\BpcAndEmergencyAidService;

class BpcAndEmergencyAid
{
    public function __construct(
        private BpcAndEmergencyAidRepository $bpcAndEmergencyAidRepository,
        private BpcAndEmergencyAidService $bpcAndEmergencyAidService
    ) {
    }

    public function getBpcAndEmergencyAidByCity(string $ibgeCode, DateTime $monthYear, int $pageNumber): array
    {
        $bpc = $this->getBpcFromDbOrApi($ibgeCode, $monthYear, $pageNumber);
        $emergencyAid = $this->getEmergencyAidFromDbOrApi($ibgeCode, $monthYear, $pageNumber);

        return [
            'bpc' => $bpc,
            'emergency_aid' => $emergencyAid,
        ];
    }

    private function getBpcFromDbOrApi(string $ibgeCode, DateTime $monthYear, int $pageNumber): array
    {
        $bpc = $this->bpcAndEmergencyAidRepository->findBpc($ibgeCode, $monthYear);

        if (is_null($bpc)) {
            $bpc = $this->bpcAndEmergencyAidService->getBpcByCity($ibgeCode, $monthYear, $pageNumber);
            $this->bpcAndEmergencyAidRepository->saveBpcOrEmergencyAid(
                $bpc['id_ref'],
                new DateTime($bpc['data_referencia']),
                $bpc['municipio_codigo_ibge'],
                $bpc['municipio_nome_ibge'],
                $bpc['municipio_codigo_regiao'],
                $bpc['municipio_nome_regiao'],
                $bpc['municipio_pais'],
                $bpc['municipio_uf_sigla'],
                $bpc['municipio_uf_nome'],
                $bpc['tipo_id'],
                $bpc['tipo_descricao'],
                $bpc['tipo_descricao_detalhada'],
                $bpc['valor_centavos'],
                $bpc['quantidade_beneficiados']
            );
        }

        return $bpc;
    }

    private function getEmergencyAidFromDbOrApi(string $ibgeCode, DateTime $monthYear, int $pageNumber): array
    {
        $emergencyAid = $this->bpcAndEmergencyAidRepository->findEmergencyAid($ibgeCode, $monthYear);
        if (is_null($emergencyAid)) {
            $emergencyAid = $this->bpcAndEmergencyAidService->getEmergencyAidByCity($ibgeCode, $monthYear, $pageNumber);
            $this->bpcAndEmergencyAidRepository->saveBpcOrEmergencyAid(
                $emergencyAid['id_ref'],
                new DateTime($emergencyAid['data_referencia']),
                $emergencyAid['municipio_codigo_ibge'],
                $emergencyAid['municipio_nome_ibge'],
                $emergencyAid['municipio_codigo_regiao'],
                $emergencyAid['municipio_nome_regiao'],
                $emergencyAid['municipio_pais'],
                $emergencyAid['municipio_uf_sigla'],
                $emergencyAid['municipio_uf_nome'],
                $emergencyAid['tipo_id'],
                $emergencyAid['tipo_descricao'],
                $emergencyAid['tipo_descricao_detalhada'],
                $emergencyAid['valor_centavos'],
                $emergencyAid['quantidade_beneficiados']
            );
        }

        return $emergencyAid;
    }
}
