<?php

namespace Intec\TransparenciaViagensServico\Repository;

use DateTime;
use Intec\IntecSlimBase\Exception\Domain\GenericDomainException;
use PDO;

class BpcAndEmergencyAidRepository implements RepositoryInterface
{
    private const TYPE_BPC = 5;
    private const TYPE_EMERGENCY_AID = 6;

    public function __construct(private PDO $pdo)
    {
    }

    public function findEmergencyAid(string $ibgeCode, DateTime $monthYear): ?array
    {
        $result = $this->getBpcOrEmergencyAid($ibgeCode, $monthYear, self::TYPE_EMERGENCY_AID);

        return $result ? $result : null;
    }

    public function findBpc(string $ibgeCode, DateTime $monthYear): ?array
    {
        $result = $this->getBpcOrEmergencyAid($ibgeCode, $monthYear, self::TYPE_BPC);

        return $result ? $result : null;
    }

    public function saveBpcOrEmergencyAid(
        int $idRef,
        DateTime $refDate,
        string $ibgeCode,
        string $ibgeName,
        string $regionCode,
        string $regionName,
        string $country,
        string $stateCode,
        string $stateName,
        int $type,
        string $typeDesc,
        string $typeLongDesc,
        int $valueInCents,
        int $amountAided
    ): int {
        $params = [
            'id_ref' => $idRef,
            'data_referencia' => $refDate->format('Y-m-d'),
            'municipio_codigo_ibge' => $ibgeCode,
            'municipio_nome_ibge' => $ibgeName,
            'municipio_codigo_regiao' => $regionCode,
            'municipio_nome_regiao' => $regionName,
            'municipio_pais' => $country,
            'municipio_uf_sigla' => $stateCode,
            'municipio_uf_nome' => $stateName,
            'tipo_id' => $type,
            'tipo_descricao' => $typeDesc,
            'tipo_descricao_detalhada' => $typeLongDesc,
            'valor_centavos' => $valueInCents,
            'quantidade_beneficiados' => $amountAided,
        ];

        $stmt = $this->pdo->prepare(
            'insert into aid(
                id_ref,
                data_referencia,
                municipio_codigo_ibge,
                municipio_nome_ibge,
                municipio_codigo_regiao,
                municipio_nome_regiao,
                municipio_pais,
                municipio_uf_sigla,
                municipio_uf_nome,
                tipo_id,
                tipo_descricao,
                tipo_descricao_detalhada,
                valor_centavos,
                quantidade_beneficiados
            ) values(
                :id_ref,
                :data_referencia,
                :municipio_codigo_ibge,
                :municipio_nome_ibge,
                :municipio_codigo_regiao,
                :municipio_nome_regiao,
                :municipio_pais,
                :municipio_uf_sigla,
                :municipio_uf_nome,
                :tipo_id,
                :tipo_descricao,
                :tipo_descricao_detalhada,
                :valor_centavos,
                :quantidade_beneficiados
            )',
            $params
        );

        $stmt->execute($params);

        return $stmt->rowCount();
    }

    private function getBpcOrEmergencyAid(string $ibgeCode, DateTime $monthYear, int $type): false|array
    {
        $params = [
            $ibgeCode,
            $monthYear->format('Y-m-01'),
            $type,
        ];

        $sth = $this->pdo->prepare(
            'select
            *
            from aid
            where
                municipio_codigo_ibge = ?
                and data_referencia = ?
                and tipo_id = ?'
        );

        $sth->execute($params);

        return $sth->fetch();
    }
}
