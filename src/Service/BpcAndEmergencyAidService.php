<?php

namespace Intec\TransparenciaViagensServico\Service;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use Intec\IntecSlimBase\Exception\Domain\GenericDomainException;
use Intec\TransparenciaViagensServico\Helper\HttpGetTrait;

class BpcAndEmergencyAidService
{
    use HttpGetTrait;

    private const EMERGENCY_AID_BY_CITY = '/api-de-dados/auxilio-emergencial-por-municipio';
    private const BPC_BY_CITY = '/api-de-dados/bpc-por-municipio';

    public function __construct(private Client $httpClient)
    {
    }

    public function getEmergencyAidByCity(string $ibgeCode, DateTime $monthYear, int $pageNumber): ?array
    {
        $queryParams = [
            'codigoIbge' => $ibgeCode,
            'mesAno' => $monthYear->format('Ym'),
            'pagina' => $pageNumber,
        ];

        try {
            $result = $this->get(self::EMERGENCY_AID_BY_CITY, $queryParams);

            if (!$result) {
                throw new Exception('Empty emergency aid');
            }

            return $this->formatBpcOrEmergencyAid($result[0]);
        } catch (Exception $e) {
            throw new GenericDomainException($queryParams, $e->getMessage(), 100_000_200, $e);
        }
    }

    public function getBpcByCity(string $ibgeCode, DateTime $monthYear, int $pageNumber): ?array
    {
        $queryParams = [
            'codigoIbge' => $ibgeCode,
            'mesAno' => $monthYear->format('Ym'),
            'pagina' => $pageNumber,
        ];

        try {
            $result = $this->get(self::BPC_BY_CITY, $queryParams);

            if (!$result) {
                throw new Exception('Empty bpc');
            }

            return $this->formatBpcOrEmergencyAid($result[0]);
        } catch (Exception $e) {
            throw new GenericDomainException($queryParams, $e->getMessage(), 100_000_300, $e);
        }
    }

    private function formatBpcOrEmergencyAid(array $result)
    {
        $city = $result['municipio'];
        $type = $result['tipo'];

        return [
            'id_ref' => $result['id'],
            'data_referencia' => $result['dataReferencia'],
            'municipio_codigo_ibge' => $city['codigoIBGE'],
            'municipio_nome_ibge' => $city['nomeIBGE'],
            'municipio_codigo_regiao' => $city['codigoRegiao'],
            'municipio_nome_regiao' => $city['nomeRegiao'],
            'municipio_pais' => $city['pais'],
            'municipio_uf_sigla' => $city['uf']['sigla'],
            'municipio_uf_nome' => $city['uf']['nome'],
            'tipo_id' => $type['id'],
            'tipo_descricao' => $type['descricao'],
            'tipo_descricao_detalhada' => $type['descricaoDetalhada'],
            'valor_centavos' => (int) (100 * $result['valor']),
            'quantidade_beneficiados' => $result['quantidadeBeneficiados'],
        ];
    }
}
