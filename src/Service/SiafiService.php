<?php

namespace Intec\TransparenciaViagensServico\Service;

use Exception;
use GuzzleHttp\Client;
use Intec\IntecSlimBase\Exception\Domain\GenericDomainException;
use Intec\TransparenciaViagensServico\Helper\HttpGetTrait;

class SiafiService
{
    use HttpGetTrait;

    private const SIAFI_CODES = '/api-de-dados/orgaos-siafi';

    public function __construct(private Client $httpClient)
    {
    }

    public function getSiafiCodes(int $pageNumber): ?array
    {
        $queryParams = [
            'pagina' => $pageNumber,
        ];

        try {
            return $this->get(self::SIAFI_CODES, $queryParams);
        } catch (Exception $e) {
            throw new GenericDomainException($queryParams, $e->getMessage(), 100_000_100, $e);
        }
    }
}
