<?php

namespace Intec\TransparenciaViagensServico\Test\Functional\Action;

use Intec\TransparenciaViagensServico\Test\TestCase;

final class GetBpcAndEmergencyAidFTest extends TestCase
{
    public function testGetBpcAndEmergencyAidWillReturn200(): void
    {
        // $siafi = [
        //     'id' => 1,
        //     'siafi_code' => '02000',
        //     'siafi_description' => 'Senado Federal - Unidades com vínculo direto',
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'updated_at' => date('Y-m-d H:i:s'),
        // ];

        // $siafiRepositoryMock = $this->createMock(SiafiRepository::class);
        // $siafiRepositoryMock
        //     ->expects($this->once())
        //     ->method('findSiafiByCode')
        //     ->with($siafi['siafi_code'])
        //     ->willReturn($siafi);

        // $this->container->set(SiafiRepository::class, $siafiRepositoryMock);

        $resp = $this->runApp('GET', '/transparencia/bpc-and-emergency-aid?ibge_code=3132404&month_year=202004&page_number=1');

        $this->assertEquals(200, $resp->getStatusCode());
        $result = $this->decodeResponse($resp)['data'];

        $this->assertArrayHasKey('bpc', $result);
        $this->assertArrayHasKey('emergency_aid', $result);
    }
}
